<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:16 PM
 */

namespace App\UseCase;

interface IGetEvolutionsUseCaseInput {

    /**
     * Returns GUID of the rarity to use to filter the data set
     *
     * @return null|string
     */
    public function getRarityId(): ?string;

    /**
     * Returns GUID of the size to use to filter the data set
     *
     * @return null|string
     */
    public function getSizeId(): ?string;

    /**
     * Sets the GUID of the rarity to use to filter the data set
     *
     * @param null|string $rarityId
     */
    public function setRarityId(?string $rarityId): void;

    /**
     * Sets the GUID of the size to use to filter the data set
     *
     * @param null|string $sizeId
     */
    public function setSizeId(?string $sizeId): void;

}