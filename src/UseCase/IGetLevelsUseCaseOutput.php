<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:16 PM
 */

namespace App\UseCase;

use App\Entity\Level;

interface IGetLevelsUseCaseOutput {

    /**
     * @return Level[]
     */
    function getLevels(): array;

    /**
     * @param Level[] $levels
     */
    function setLevels(array $levels): void;

}