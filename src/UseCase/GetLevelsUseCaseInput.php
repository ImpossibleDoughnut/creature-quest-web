<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

class GetLevelsUseCaseInput implements IGetLevelsUseCaseInput {

    /**
     * @var string
     */
    private $evolutionId;

    /**
     * @var string
     */
    private $sizeId;

    /**
     * Returns GUID of the evolution to use to filter the data set
     *
     * @return null|string
     */
    public function getEvolutionId(): ?string {
        return $this->evolutionId;
    }

    /**
     * Returns GUID of the size to use to filter the data set
     *
     * @return null|string
     */
    public function getSizeId(): ?string {
        return $this->sizeId;
    }

    /**
     * Sets the GUID of the evolution to use to filter the data set
     *
     * @param null|string $evolutionId
     */
    public function setEvolutionId(?string $evolutionId): void {
        $this->evolutionId = $evolutionId;
    }

    /**
     * Sets the GUID of the size to use to filter the data set
     *
     * @param null|string $sizeId
     */
    public function setSizeId(?string $sizeId): void {
        $this->sizeId = $sizeId;
    }
}