<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:54 AM
 */

namespace App\UseCase;

use App\EntityGateway\IGetManyCreatureGateway;

class GetCreaturesUseCase implements IGetCreaturesUseCase {

    /**
     * @var IGetManyCreatureGateway
     */
    private $getAllCreaturesEntityGateway;

    public function __construct(IGetManyCreatureGateway $getAllCreaturesEntityGateway) {
        $this->getAllCreaturesEntityGateway = $getAllCreaturesEntityGateway;
    }

    public function execute(IGetCreaturesUseCaseInput $input, IGetCreaturesUseCaseOutput $output): void {
        $colorFilters = [];

        if (!empty($input->getColorId())) {
            $colorFilters[] = $input->getColorId();
        }

        $creatures = $this->getAllCreaturesEntityGateway->execute($colorFilters);

        $output->setCreatures($creatures);
    }
}