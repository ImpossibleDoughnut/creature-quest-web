<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

class GetEvolutionsUseCaseInput implements IGetEvolutionsUseCaseInput {

    /**
     * @var string
     */
    private $rarityId;

    /**
     * @var string
     */
    private $sizeId;

    /**
     * Returns GUID of the rarity to use to filter the data set
     *
     * @return null|string
     */
    public function getRarityId(): ?string {
        return $this->rarityId;
    }

    /**
     * Returns GUID of the size to use to filter the data set
     *
     * @return null|string
     */
    public function getSizeId(): ?string {
        return $this->sizeId;
    }

    /**
     * Sets the GUID of the rarity to use to filter the data set
     *
     * @param null|string $rarityId
     */
    public function setRarityId(?string $rarityId): void {
        $this->rarityId = $rarityId;
    }

    /**
     * Sets the GUID of the size to use to filter the data set
     *
     * @param null|string $sizeId
     */
    public function setSizeId(?string $sizeId): void {
        $this->sizeId = $sizeId;
    }
}