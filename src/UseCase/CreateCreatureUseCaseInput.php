<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/10/2018
 * Time: 4:11 PM
 */

namespace App\UseCase;

class CreateCreatureUseCaseInput implements ICreateCreatureUseCaseInput {

    /**
     * @var string
     */
    private $attack;
    /**
     * @var int
     */
    private $attackRating;
    /**
     * @var string
     */
    private $color;
    /**
     * @var int
     */
    private $defenseRating;
    /**
     * @var string
     */
    private $evolution;
    /**
     * @var int
     */
    private $healthPoints;
    /**
     * @var string
     */
    private $level;
    /**
     * @var int
     */
    private $luckRating;
    /**
     * @var int
     */
    private $manaPool;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $nextCreatureId;
    /**
     * @var int
     */
    private $powerRating;
    /**
     * @var string
     */
    private $previousCreatureId;
    /**
     * @var string
     */
    private $rarity;
    /**
     * @var string
     */
    private $size;

    /**
     * @return string
     */
    public function getAttackId(): ?string {
        return $this->attack;
    }

    public function getAttackRating(): ?int {
        return $this->attackRating;
    }

    /**
     * @return string
     */
    public function getColorId(): ?string {
        return $this->color;
    }

    public function getDefenseRating(): ?int {
        return $this->defenseRating;
    }

    /**
     * @return string
     */
    public function getEvolutionId(): ?string {
        return $this->evolution;
    }

    public function getHealthPoints(): ?int {
        return $this->healthPoints;
    }

    /**
     * @return string
     */
    function getLevelId(): ?string {
        return $this->level;
    }

    public function getLuckRating(): ?int {
        return $this->luckRating;
    }

    public function getManaPool(): ?int {
        return $this->manaPool;
    }

    /**
     * @return string
     */
    public function getName(): ?string {
        return $this->name;
    }

    public function getNextCreatureId(): ?string {
        return $this->nextCreatureId;
    }

    public function getPowerRating(): ?int {
        return $this->powerRating;
    }

    public function getPreviousCreatureId(): ?string {
        return $this->previousCreatureId;
    }

    /**
     * @return string
     */
    public function getRarityId(): ?string {
        return $this->rarity;
    }

    /**
     * @return string
     */
    public function getSizeId(): ?string {
        return $this->size;
    }

    /**
     * @param string $attack
     */
    public function setAttackId(?string $attack): void {
        $this->attack = $attack;
    }

    public function setAttackRating(?int $attackRating): void {
        $this->attackRating = $attackRating;
    }

    /**
     * @param string $color
     */
    public function setColorId(?string $color): void {
        $this->color = $color;
    }

    public function setDefenseRating(?int $defenseRating): void {
        $this->defenseRating = $defenseRating;
    }

    /**
     * @param string $evolution
     */
    public function setEvolutionId(?string $evolution): void {
        $this->evolution = $evolution;
    }

    public function setHealthPoints(?int $healthPoints): void {
        $this->healthPoints = $healthPoints;
    }

    /**
     * @param string $level
     */
    function setLevelId(?string $level): void {
        $this->level = $level;
    }

    public function setLuckRating(?int $luckRating): void {
        $this->luckRating = $luckRating;
    }

    public function setManaPool(?int $manaPool): void {
        $this->manaPool = $manaPool;
    }

    /**
     * @param string $name
     */
    public function setName(?string $name): void {
        $this->name = $name;
    }

    public function setNextCreatureId(?string $nextCreatureId): void {
        $this->nextCreatureId = $nextCreatureId;
    }

    public function setPowerRating(?int $powerRating): void {
        $this->powerRating = $powerRating;
    }

    public function setPreviousCreatureId(?string $previousCreatureId): void {
        $this->previousCreatureId = $previousCreatureId;
    }

    /**
     * @param string $rarity
     */
    public function setRarityId(?string $rarity): void {
        $this->rarity = $rarity;
    }

    /**
     * @param string $size
     */
    public function setSizeId(?string $size): void {
        $this->size = $size;
    }
}