<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:16 PM
 */

namespace App\UseCase;

use App\Entity\Size;

interface IGetSizesUseCaseOutput {

    /**
     * @return Size[]
     */
    function getSizes(): array;

    /**
     * @param Size[] $sizes
     */
    function setSizes(array $sizes): void;

}