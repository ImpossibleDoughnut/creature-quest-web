<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 7/7/2018
 * Time: 2:52 PM
 */

namespace App\UseCase;

use App\Entity\Creature;

interface IGetNextEvolutionOptionsUseCaseOutput {
    /**
     * @return Creature[]
     */
    function getCreatures(): array;

    /**
     * @param Creature[] $creatures
     */
    function setCreatures(array $creatures): void;
}