<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/10/2018
 * Time: 4:11 PM
 */

namespace App\UseCase;

use App\Entity\Creature;

class CreateCreatureUseCaseOutput implements ICreateCreatureUseCaseOutput {

    /**
     * @var Creature
     */
    private $creature;

    function getCreature(): Creature {
        return $this->creature;
    }

    function setCreature(Creature $entity): void {
        $this->creature = $entity;
    }
}