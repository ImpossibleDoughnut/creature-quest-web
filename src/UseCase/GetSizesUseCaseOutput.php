<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\Entity\Size;

class GetSizesUseCaseOutput implements IGetSizesUseCaseOutput {

    /**
     * @var Size[]
     */
    private $sizes;

    public function __construct() {
        $this->sizes = [];
    }

    /**
     * @return Size[]
     */
    function getSizes(): array {
        return $this->sizes;
    }

    /**
     * @param Size[] $sizes
     */
    function setSizes(array $sizes): void {
        $this->sizes = $sizes;
    }
}