<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\Entity\Rarity;

class GetRaritiesUseCaseOutput implements IGetRaritiesUseCaseOutput {

    /**
     * @var Rarity[]
     */
    private $rarities;

    public function __construct() {
        $this->rarities = [];
    }

    /**
     * @return Rarity[]
     */
    function getRarities(): array {
        return $this->rarities;
    }

    /**
     * @param Rarity[] $rarities
     */
    function setRarities(array $rarities): void {
        $this->rarities = $rarities;
    }
}