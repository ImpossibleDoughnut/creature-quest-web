<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 7/7/2018
 * Time: 2:52 PM
 */

namespace App\UseCase;

interface IGetPreviousEvolutionOptionsUseCase {
    function execute(
        IGetPreviousEvolutionOptionsUseCaseInput $input,
        IGetPreviousEvolutionOptionsUseCaseOutput $output
    ): void;
}