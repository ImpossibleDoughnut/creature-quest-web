<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 1:02 AM
 */

namespace App\Entity;

class Creature implements IEntity {

    /**
     * @var Attack
     */
    private $attack;
    /**
     * @var int
     */
    private $attackRating;
    /**
     * @var Color
     */
    private $color;
    /**
     * @var int
     */
    private $defenseRating;
    /**
     * @var Evolution
     */
    private $evolution;
    /**
     * @var int
     */
    private $healthPoints;
    /**
     * @var string
     */
    private $id;
    /**
     * @var Level
     */
    private $level;
    /**
     * @var int
     */
    private $luckRating;
    /**
     * @var int
     */
    private $manaPool;
    /**
     * @var string
     */
    private $name;
    /**
     * @var Creature
     */
    private $nextCreature;
    /**
     * @var int
     */
    private $powerRating;
    /**
     * @var Creature
     */
    private $previousCreature;
    /**
     * @var Rarity
     */
    private $rarity;
    /**
     * @var Size
     */
    private $size;

    /**
     * @return Attack
     */
    public function getAttack(): ?Attack {
        return $this->attack;
    }

    /**
     * @return int
     */
    public function getAttackRating(): ?int {
        return $this->attackRating;
    }

    /**
     * @return Color
     */
    public function getColor(): ?Color {
        return $this->color;
    }

    /**
     * @return int
     */
    public function getDefenseRating(): ?int {
        return $this->defenseRating;
    }

    /**
     * @return Evolution
     */
    public function getEvolution(): ?Evolution {
        return $this->evolution;
    }

    /**
     * @return int
     */
    public function getHealthPoints(): ?int {
        return $this->healthPoints;
    }

    /**
     * @return string
     */
    public function getId(): ?string {
        return $this->id;
    }

    /**
     * @return Level
     */
    public function getLevel(): ?Level {
        return $this->level;
    }

    /**
     * @return int
     */
    public function getLuckRating(): ?int {
        return $this->luckRating;
    }

    /**
     * @return int
     */
    public function getManaPool(): ?int {
        return $this->manaPool;
    }

    /**
     * @return string
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @return Creature
     */
    public function getNextCreature(): ?Creature {
        return $this->nextCreature;
    }

    /**
     * @return int
     */
    public function getPowerRating(): ?int {
        return $this->powerRating;
    }

    /**
     * @return Creature
     */
    public function getPreviousCreature(): ?Creature {
        return $this->previousCreature;
    }

    /**
     * @return Rarity
     */
    public function getRarity(): ?Rarity {
        return $this->rarity;
    }

    /**
     * @return Size
     */
    public function getSize(): ?Size {
        return $this->size;
    }

    /**
     * @param Attack $attack
     */
    public function setAttack(?Attack $attack) {
        $this->attack = $attack;
    }

    /**
     * @param int $attackRating
     */
    public function setAttackRating(?int $attackRating) {
        $this->attackRating = $attackRating;
    }

    /**
     * @param Color $color
     */
    public function setColor(?Color $color) {
        $this->color = $color;
    }

    /**
     * @param int $defenseRating
     */
    public function setDefenseRating(?int $defenseRating) {
        $this->defenseRating = $defenseRating;
    }

    /**
     * @param Evolution $evolution
     */
    public function setEvolution(?Evolution $evolution) {
        $this->evolution = $evolution;
    }

    /**
     * @param int $healthPoints
     */
    public function setHealthPoints(?int $healthPoints) {
        $this->healthPoints = $healthPoints;
    }

    /**
     * @param string $id
     */
    public function setId(?string $id) {
        $this->id = $id;
    }

    /**
     * @param Level $level
     */
    public function setLevel(?Level $level) {
        $this->level = $level;
    }

    /**
     * @param int $luckRating
     */
    public function setLuckRating(?int $luckRating) {
        $this->luckRating = $luckRating;
    }

    /**
     * @param int $manaPool
     */
    public function setManaPool(?int $manaPool) {
        $this->manaPool = $manaPool;
    }

    /**
     * @param string $name
     */
    public function setName(?string $name) {
        $this->name = $name;
    }

    /**
     * @param Creature $nextCreature
     */
    public function setNextCreature(?Creature $nextCreature) {
        $this->nextCreature = $nextCreature;
    }

    /**
     * @param int $powerRating
     */
    public function setPowerRating(?int $powerRating) {
        $this->powerRating = $powerRating;
    }

    /**
     * @param Creature $previousCreature
     */
    public function setPreviousCreature(?Creature $previousCreature) {
        $this->previousCreature = $previousCreature;
    }

    /**
     * @param Rarity $rarity
     */
    public function setRarity(?Rarity $rarity) {
        $this->rarity = $rarity;
    }

    /**
     * @param Size $size
     */
    public function setSize(?Size $size) {
        $this->size = $size;
    }

}