<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:22 PM
 */

namespace App\Entity;

class Evolution implements IEntity {

    /**
     * @var int
     */
    private $cardinality;
    /**
     * @var string
     */
    private $id;
    /**
     * @var Rarity[]
     */
    private $rarities;
    /**
     * @var Size[]
     */
    private $sizes;

    /**
     * @return int
     */
    public function getCardinality(): int {
        return $this->cardinality;
    }

    /**
     * @return string
     */
    public function getId(): ?string {
        return $this->id;
    }

    /**
     * @return Rarity[]
     */
    public function getRarities(): array {
        return $this->rarities;
    }

    /**
     * @return Size[]
     */
    public function getSizes(): array {
        return $this->sizes;
    }

    /**
     * @param int $cardinality
     */
    public function setCardinality(int $cardinality) {
        $this->cardinality = $cardinality;
    }

    /**
     * @param string $id
     */
    public function setId(?string $id) {
        $this->id = $id;
    }

    /**
     * @param Rarity[] $rarities
     */
    public function setRarities(array $rarities) {
        $this->rarities = $rarities;
    }

    /**
     * @param Size[] $sizes
     */
    public function setSizes(array $sizes) {
        $this->sizes = $sizes;
    }

}