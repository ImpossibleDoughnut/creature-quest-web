<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/31/18
 * Time: 2:13 PM
 */

namespace App\EntityMapper;

use App\Entity\Attack;
use App\Entity\Color;
use App\Entity\Creature;
use App\Entity\Evolution;
use App\Entity\IEntity;
use App\Entity\IEntityFactory;
use App\Entity\Level;
use App\Entity\Rarity;
use App\Entity\Size;
use InvalidArgumentException;

class CreatureMapper extends EntityMapper {

    static $EntityClassName = Creature::class;
    static $SourceKeyAttack = 'attack';
    static $SourceKeyAttackRating = 'attackRating';
    static $SourceKeyColor = 'color';
    static $SourceKeyDefenseRating = 'defenseRating';
    static $SourceKeyEvolution = 'evolution';
    static $SourceKeyHealthPoints = 'healthPoints';
    static $SourceKeyLevel = 'level';
    static $SourceKeyLuckRating = 'luckRating';
    static $SourceKeyManaPool = 'manaPool';
    static $SourceKeyName = 'name';
    static $SourceKeyNextCreature = 'nextCreature';
    static $SourceKeyPowerRating = 'powerRating';
    static $SourceKeyPreviousCreature = 'previousCreature';
    static $SourceKeyRarity = 'rarity';
    static $SourceKeySize = 'size';
    /**
     * @var AttackMapper
     */
    private $attackMapper;

    /**
     * @var ColorMapper
     */
    private $colorMapper;

    /**
     * @var EvolutionMapper
     */
    private $evolutionMapper;

    /**
     * @var LevelMapper
     */
    private $levelMapper;

    /**
     * @var RarityMapper
     */
    private $rarityMapper;

    /**
     * @var SizeMapper
     */
    private $sizeMapper;

    function __construct(
        IEntityFactory $entityFactory,
        AttackMapper $attackMapper,
        ColorMapper $colorMapper,
        EvolutionMapper $evolutionMapper,
        LevelMapper $levelMapper,
        RarityMapper $rarityMapper,
        SizeMapper $sizeMapper
    ) {
        parent::__construct($entityFactory);

        $this->attackMapper = $attackMapper;
        $this->colorMapper = $colorMapper;
        $this->evolutionMapper = $evolutionMapper;
        $this->levelMapper = $levelMapper;
        $this->rarityMapper = $rarityMapper;
        $this->sizeMapper = $sizeMapper;
    }

    protected function createEntity(): IEntity {
        return $this->getEntityFactory()->create(self::$EntityClassName);
    }

    private function mapAttack(array $input, Creature &$output): void {
        $entity = $this->getEntityFactory()->create(Attack::class);

        if (isset($input[self::$SourceKeyAttack]) && $input[self::$SourceKeyAttack] != null) {
            try {
                $this->attackMapper->mapOne($input[self::$SourceKeyAttack], $entity);
            } catch (InvalidArgumentException $e) {
                $entity = null;
            }
        }

        if ($entity instanceof Attack) {
            $output->setAttack($entity);
        }
    }

    private function mapColor(array $input, Creature &$output): void {
        $entity = $this->getEntityFactory()->create(Color::class);

        if (isset($input[self::$SourceKeyColor]) && $input[self::$SourceKeyColor] != null) {
            try {
                $this->colorMapper->mapOne($input[self::$SourceKeyColor], $entity);
            } catch (InvalidArgumentException $e) {
                $entity = null;
            }
        }

        if ($entity instanceof Color) {
            $output->setColor($entity);
        }
    }

    private function mapEvolution(array $input, Creature &$output): void {
        $entity = $this->getEntityFactory()->create(Evolution::class);

        if (isset($input[self::$SourceKeyEvolution]) && $input[self::$SourceKeyEvolution] != null) {
            try {
                $this->evolutionMapper->mapOne($input[self::$SourceKeyEvolution], $entity);
            } catch (InvalidArgumentException $e) {
                $entity = null;
            }
        }

        if ($entity instanceof Evolution) {
            $output->setEvolution($entity);
        }
    }

    private function mapLevel(array $input, Creature &$output): void {
        $entity = $this->getEntityFactory()->create(Level::class);

        if (isset($input[self::$SourceKeyLevel]) && $input[self::$SourceKeyLevel] != null) {
            try {
                $this->levelMapper->mapOne($input[self::$SourceKeyLevel], $entity);
            } catch (InvalidArgumentException $e) {
                $entity = null;
            }
        }

        if ($entity instanceof Level) {
            $output->setLevel($entity);
        }
    }

    private function mapNextCreature(array $input, Creature &$output): void {
        $entity = $this->getEntityFactory()->create(Creature::class);

        if (isset($input[self::$SourceKeyNextCreature]) && $input[self::$SourceKeyNextCreature] != null) {
            try {
                $this->mapOne($input[self::$SourceKeyNextCreature], $entity);
            } catch (InvalidArgumentException $e) {
                $entity = null;
            }
        }

        if ($entity instanceof Creature) {
            $output->setNextCreature($entity);
        }
    }

    /**
     * @param string[] $input
     * @param IEntity  $output
     *
     * @throws InvalidArgumentException
     */
    public function mapOne(array $input, IEntity &$output): void {
        $id = $input[self::$SourceKeyId];
        $attackRating = $input[self::$SourceKeyAttackRating] ?? null;
        $defenseRating = $input[self::$SourceKeyDefenseRating] ?? null;
        $healthPoints = $input[self::$SourceKeyHealthPoints] ?? null;
        $luckRating = $input[self::$SourceKeyLuckRating] ?? null;
        $manaPool = $input[self::$SourceKeyManaPool] ?? null;
        $name = $input[self::$SourceKeyName] ?? null;
        $powerRating = $input[self::$SourceKeyPowerRating] ?? null;

        if ($output instanceof Creature) {
            $output->setId($id);
            $output->setAttackRating($attackRating);
            $output->setDefenseRating($defenseRating);
            $output->setHealthPoints($healthPoints);
            $output->setLuckRating($luckRating);
            $output->setManaPool($manaPool);
            $output->setName($name);
            $output->setPowerRating($powerRating);

            $this->mapAttack($input, $output);
            $this->mapColor($input, $output);
            $this->mapEvolution($input, $output);
            $this->mapLevel($input, $output);
            $this->mapNextCreature($input, $output);
            $this->mapPreviousCreature($input, $output);
            $this->mapRarity($input, $output);
            $this->mapSize($input, $output);
        } else {
            throw new InvalidArgumentException(
                sprintf('expected output type `%s` but received `%s`', self::$EntityClassName, get_class($output))
            );
        }
    }

    private function mapPreviousCreature(array $input, Creature &$output): void {
        $entity = $this->getEntityFactory()->create(Creature::class);

        if (isset($input[self::$SourceKeyPreviousCreature]) && $input[self::$SourceKeyPreviousCreature] != null) {
            try {
                $this->mapOne($input[self::$SourceKeyPreviousCreature], $entity);
            } catch (InvalidArgumentException $e) {
                $entity = null;
            }
        }

        if ($entity instanceof Creature) {
            $output->setPreviousCreature($entity);
        }
    }

    private function mapRarity(array $input, Creature &$output): void {
        $entity = $this->getEntityFactory()->create(Rarity::class);

        if (isset($input[self::$SourceKeyRarity]) && $input[self::$SourceKeyRarity] != null) {
            try {
                $this->rarityMapper->mapOne($input[self::$SourceKeyRarity], $entity);
            } catch (InvalidArgumentException $e) {
                $entity = null;
            }
        }

        if ($entity instanceof Rarity) {
            $output->setRarity($entity);
        }
    }

    private function mapSize(array $input, Creature &$output): void {
        $entity = $this->getEntityFactory()->create(Size::class);

        if (isset($input[self::$SourceKeySize]) && $input[self::$SourceKeySize] != null) {
            try {
                $this->sizeMapper->mapOne($input[self::$SourceKeySize], $entity);
            } catch (InvalidArgumentException $e) {
                $entity = null;
            }
        }

        if ($entity instanceof Size) {
            $output->setSize($entity);
        }
    }
}