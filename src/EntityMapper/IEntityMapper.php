<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/31/18
 * Time: 2:13 PM
 */

namespace App\EntityMapper;

use App\Entity\IEntity;

interface IEntityMapper {
    /**
     * @param array[]   $input
     * @param IEntity[] $output
     */
    function mapMany(array $input, array &$output): void;

    /**
     * @param string[] $input
     * @param IEntity  $output
     */
    function mapOne(array $input, IEntity &$output): void;
}