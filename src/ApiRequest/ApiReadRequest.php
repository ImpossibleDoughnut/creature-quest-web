<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/31/18
 * Time: 1:57 PM
 */

namespace App\ApiRequest;

use Exception;

class ApiReadRequest extends ApiRequest implements IApiReadRequest {

    /**
     * @param IApiReadRequestConfiguration $apiRequestConfiguration
     *
     * @return array
     * @throws Exception
     */
    public function execute(IApiReadRequestConfiguration $apiRequestConfiguration): array {
        $curl = curl_init();
        $url = $apiRequestConfiguration->getEndpointUrl();
        $url .= $this->implodeParameters($apiRequestConfiguration->getParameters());

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($curl);
        curl_close($curl);

        $decodedData = json_decode($data, true);

        if (isset($decodedData[self::API_REQUEST_KEY_DATA])) {
            $decodedData = $decodedData[self::API_REQUEST_KEY_DATA];
        } else {
            throw new Exception(implode(PHP_EOL, $decodedData[self::API_REQUEST_KEY_ERRORS]));
        }

        return $decodedData;
    }
}