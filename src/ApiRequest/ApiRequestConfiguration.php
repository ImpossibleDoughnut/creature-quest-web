<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 12:34 PM
 */

namespace App\ApiRequest;

abstract class ApiRequestConfiguration implements IApiRequestConfiguration {

    /**
     * @var string
     */
    private $endpointUrlSuffix;

    /**
     * @var array
     */
    private $parameters;

    public function __construct(string $endpointUrlSuffix = '', array $parameters = []) {
        $this->endpointUrlSuffix = $endpointUrlSuffix;
        $this->parameters = $parameters;
    }

    private function getApiUrl(): string {
        return 'http://cq-api:8080/';
    }

    public function getEndpointUrl(): string {
        $endpointUrl = $this->getApiUrl() . $this->endpointUrlSuffix;

        return $endpointUrl;
    }

    public function getEndpointUrlSuffix(): string {
        return $this->endpointUrlSuffix;
    }

    public function getParameters(): array {
        return $this->parameters;
    }
}