<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 7:25 PM
 */

namespace App\ApiRequest;

interface IApiDeleteRequest {

    function execute(IApiDeleteRequestConfiguration $apiRequestConfiguration): array;

}