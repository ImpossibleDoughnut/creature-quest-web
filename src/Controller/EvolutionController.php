<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/21/2018
 * Time: 1:18 AM
 */

namespace App\Controller;

use App\UseCase\GetEvolutionsUseCaseInput;
use App\UseCase\GetEvolutionsUseCaseOutput;
use App\UseCase\IGetEvolutionsUseCase;
use Symfony\Component\HttpFoundation\Request;

class EvolutionController extends CQController {

    /**
     * @var IGetEvolutionsUseCase
     */
    private $getEvolutionsUseCase;

    public function __construct(IGetEvolutionsUseCase $getEvolutionsUseCase) {
        $this->getEvolutionsUseCase = $getEvolutionsUseCase;
    }

    public function handleGetRequest(Request $request) {
        $evolutionsUseCaseInput = new GetEvolutionsUseCaseInput();
        $evolutionsUseCaseOutput = new GetEvolutionsUseCaseOutput();
        $rarity = $request->get('rarity');
        $size = $request->get('size');

        $evolutionsUseCaseInput->setRarityId($rarity);
        $evolutionsUseCaseInput->setSizeId($size);

        $this->getEvolutionsUseCase->execute($evolutionsUseCaseInput, $evolutionsUseCaseOutput);

        return $this->json($evolutionsUseCaseOutput);
    }

}