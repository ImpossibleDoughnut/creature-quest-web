<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:23 PM
 */

namespace App\EntityGateway;

use App\Entity\Rarity;

class GetOneRarityGateway implements IGetOneRarityGateway {

    /**
     * @var IGetManyRarityGateway
     */
    private $entityGateway;

    public function __construct(IGetManyRarityGateway $entityGateway) {
        $this->entityGateway = $entityGateway;
    }

    /**
     * @param string $id
     *
     * @return Rarity
     */
    public function getById(string $id): ?Rarity {
        $entities = $this->entityGateway->execute();
        $result = null;

        foreach ($entities as $entity) {
            if ($entity->getId() == $id) {
                $result = $entity;
                break;
            }
        }

        return $result;
    }
}