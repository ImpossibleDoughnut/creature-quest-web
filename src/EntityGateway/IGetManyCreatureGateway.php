<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 1:00 AM
 */

namespace App\EntityGateway;

use App\Entity\Creature;

interface IGetManyCreatureGateway {

    /**
     * @param string[] $colorFilters
     *
     * @return Creature[]
     */
    public function execute(array $colorFilters = []): array;
}