<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:24 PM
 */

namespace App\EntityGateway;

use App\Entity\Size;

class GetOneSizeGateway implements IGetOneSizeGateway {

    /**
     * @var IGetManySizeGateway
     */
    private $entityGateway;

    public function __construct(IGetManySizeGateway $entityGateway) {
        $this->entityGateway = $entityGateway;
    }

    /**
     * @param string $id
     *
     * @return Size
     */
    public function getById(string $id): ?Size {
        $entities = $this->entityGateway->execute();
        $result = null;

        foreach ($entities as $entity) {
            if ($entity->getId() == $id) {
                $result = $entity;
                break;
            }
        }

        return $result;
    }
}