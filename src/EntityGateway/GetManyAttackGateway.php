<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:25 PM
 */

namespace App\EntityGateway;

use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\Attack;
use App\Entity\IEntityFactory;
use App\EntityMapper\AttackMapper;

class GetManyAttackGateway implements IGetManyAttackGateway {

    /**
     * @var IApiReadRequest
     */
    protected $apiRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    protected $apiRequestConfigurationFactory;
    /**
     * @var IEntityFactory
     */
    protected $entityFactory;
    /**
     * @var AttackMapper
     */
    protected $entityMapper;

    public function __construct(
        IApiReadRequest $apiRequest,
        IApiRequestConfigurationFactory $apiRequestConfigurationFactory,
        IEntityFactory $entityFactory,
        AttackMapper $entityMapper
    ) {
        $this->apiRequest = $apiRequest;
        $this->apiRequestConfigurationFactory = $apiRequestConfigurationFactory;
        $this->entityFactory = $entityFactory;
        $this->entityMapper = $entityMapper;
    }

    /**
     * @return Attack[]
     */
    public function execute(): array {
        $endpointUrlSuffix = 'attacks';
        $apiConfiguration = $this->apiRequestConfigurationFactory->createReadConfiguration($endpointUrlSuffix);
        $entityData = $this->apiRequest->execute($apiConfiguration);
        /**
         * @todo this needs to be passed off to a class that can return an iteratable list
         */
        $entityData = $entityData[$endpointUrlSuffix];
        $entities = [];

        $this->entityMapper->MapMany($entityData, $entities);

        return $entities;
    }
}