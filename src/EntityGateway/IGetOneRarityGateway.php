<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:17 PM
 */

namespace App\EntityGateway;

use App\Entity\Rarity;

interface IGetOneRarityGateway {

    /**
     * @param string $id
     *
     * @return Rarity
     */
    public function getById(string $id): ?Rarity;

}