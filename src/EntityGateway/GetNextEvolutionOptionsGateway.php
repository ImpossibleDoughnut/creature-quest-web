<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 7/7/2018
 * Time: 2:57 PM
 */

namespace App\EntityGateway;

use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\Creature;
use App\EntityMapper\CreatureMapper;

class GetNextEvolutionOptionsGateway implements IGetNextEvolutionOptionsGateway {

    public const COLOR_PARAMETER_KEY = 'color';
    public const ENDPOINT_SUFFIX = 'creatures';
    public const ENTITY_DATA_ROOT_KEY = 'creatures';
    public const EVOLUTION_PARAMETER_KEY = 'evolution';
    public const SIZE_PARAMETER_KEY = 'size';
    /**
     * @var IApiReadRequest
     */
    private $apiRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var CreatureMapper
     */
    private $entityMapper;

    public function __construct(
        IApiReadRequest $apiRequest,
        IApiRequestConfigurationFactory $apiRequestConfigurationFactory,
        CreatureMapper $entityMapper
    ) {
        $this->apiRequest = $apiRequest;
        $this->apiRequestConfigurationFactory = $apiRequestConfigurationFactory;
        $this->entityMapper = $entityMapper;
    }

    /**
     * @param null|string $colorId
     * @param null|string $evolutionId
     * @param null|string $sizeId
     *
     * @return Creature[]
     */
    public function execute(?string $colorId, ?string $evolutionId, ?string $sizeId): array {
        $endpointUrlSuffix = self::ENDPOINT_SUFFIX;
        $parameters = [
            self::COLOR_PARAMETER_KEY => $colorId,
            self::EVOLUTION_PARAMETER_KEY => $evolutionId,
            self::SIZE_PARAMETER_KEY => $sizeId
        ];

        $apiConfiguration = $this->apiRequestConfigurationFactory->createReadConfiguration(
            $endpointUrlSuffix,
            $parameters
        );
        $entityData = $this->apiRequest->execute($apiConfiguration);
        $entityData = $entityData[self::ENTITY_DATA_ROOT_KEY];
        $entities = [];

        $this->entityMapper->MapMany($entityData, $entities);

        return $entities;
    }
}