<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:24 PM
 */

namespace App\EntityGateway;

use App\Entity\Level;

interface IGetManyLevelGateway {

    /**
     * @param string $evolutionId
     * @param string $sizeId
     *
     * @return Level[]
     */
    public function execute(?string $evolutionId = null, ?string $sizeId = null): array;
}