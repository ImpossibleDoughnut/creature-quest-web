<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:23 PM
 */

namespace App\EntityGateway;

use App\Entity\Attack;

class GetOneAttackGateway implements IGetOneAttackGateway {

    /**
     * @var IGetManyAttackGateway
     */
    private $entityGateway;

    public function __construct(IGetManyAttackGateway $entityGateway) {
        $this->entityGateway = $entityGateway;
    }

    /**
     * @param string $id
     *
     * @return Attack
     */
    public function getById(string $id): ?Attack {
        $entities = $this->entityGateway->execute();
        $result = null;

        foreach ($entities as $entity) {
            if ($entity->getId() == $id) {
                $result = $entity;
                break;
            }
        }

        return $result;
    }
}