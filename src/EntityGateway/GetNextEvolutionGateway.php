<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 7/8/2018
 * Time: 1:25 AM
 */

namespace App\EntityGateway;

use App\Entity\Evolution;

class GetNextEvolutionGateway implements IGetNextEvolutionGateway {

    /**
     * @var IGetManyEvolutionGateway
     */
    private $entityGateway;

    public function __construct(IGetManyEvolutionGateway $entityGateway) {
        $this->entityGateway = $entityGateway;
    }

    /**
     * @param string $id
     *
     * @return Evolution
     */
    public function execute(string $id): ?Evolution {
        $entities = $this->entityGateway->execute();
        $entityMap = [];
        $subject = null;
        $nextEntity = null;
        $matched = false;
        $result = null;

        foreach ($entities as $entity) {
            $entityMap[$entity->getCardinality()] = $entity;

            if ($entity->getId() == $id) {
                $subject = $entity;
            }
        }

        ksort($entityMap);

        foreach ($entityMap as $cardinality => $entity) {
            if ($matched) {
                $nextEntity = $entity;
                break;
            }

            if ($entity === $subject) {
                $matched = true;
            }
        }

        if ($matched) {
            $result = $nextEntity;
        }

        return $result;
    }
}