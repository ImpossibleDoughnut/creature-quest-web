class Mediator {

    constructor(sender, receiver) {
        this.sender = sender;
        this.receiver = receiver;

        this.sender.registerMediator(this);
    }

    mediate() {

    }
}