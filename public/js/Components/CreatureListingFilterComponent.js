class CreatureListingFilterComponent extends Component {
    constructor(documentElementId, ajaxHelper) {
        super(documentElementId, ajaxHelper);

        this.colorFilter = null;
        this.colorFilterElementName = 'color-filter';
    }

    get colorFilter() {
        return this.getState('colorFilter');
    }

    set colorFilter(colorFilter) {
        this.setState('colorFilter', colorFilter);
    }

    init() {
        super.init();
        let colorFilterElement = this.getColorFilterElement();

        colorFilterElement.on('change', () => {
            this.handleColorFilterChange();
        });
    }

    getColorFilterElement() {
        return this.documentElement.find('select[name=' + this.colorFilterElementName + ']');
    }

    handleColorFilterChange() {
        let colorFilterElement = this.getColorFilterElement();

        this.colorFilter = colorFilterElement.val();
        this.beginMediation();
    }
}