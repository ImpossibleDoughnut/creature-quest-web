class Component {
    constructor(documentElementId, ajaxHelper) {
        this.documentElementId = documentElementId;
        this.callbackUrl = null;
        this.documentElement = null;
        this.mediators = [];
        this.state = {};
        this.initalized = false;

        if (ajaxHelper instanceof AjaxHelper) {
            /**
             * @var AjaxHelper
             */
            this.ajaxHelper = ajaxHelper;
        } else {
            console.error('AjaxHelper required for component functionality!');
        }
    }

    init() {
        console.info("Initializing " + this.documentElementId);

        if (this.initalized === false) {
            this.documentElement = $(this.documentElementId);
            this.callbackUrl = this.documentElement.attr('callback');

            console.info("    Found callback URL [" + this.callbackUrl + "]");

            this.initalized = true;
        }
    }

    setState(key, value) {
        this.state[key] = value;
    }

    getState(key) {
        let result = this.state;

        if (key != null) {
            result = this.state[key];
        }

        return result;
    }

    registerMediator(mediator) {
        if (mediator instanceof Mediator) {
            this.mediators[this.mediators.length] = mediator;
        }
    }

    beginMediation() {
        for (let a = 0; a < this.mediators.length; a++) {
            let mediator = this.mediators[a];

            mediator.mediate();
        }
    }

    refresh() {
        let data = this.getState();

        this.ajaxHelper.get(this.callbackUrl, data, AjaxHelper.HTML, (data, textStatus, jqXHR) => {
            this.documentElement.html(data);
            this.init();
        });
    }
}