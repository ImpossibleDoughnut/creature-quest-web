<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:19 AM
 */

namespace App\Tests\UseCase;

use App\Entity\Attack;
use App\EntityGateway\IGetManyAttackGateway;
use App\UseCase\GetAttacksUseCase;
use App\UseCase\GetAttacksUseCaseInput;
use App\UseCase\GetAttacksUseCaseOutput;
use App\UseCase\IGetAttacksUseCase;
use App\UseCase\IGetAttacksUseCaseInput;
use App\UseCase\IGetAttacksUseCaseOutput;
use PHPUnit\Framework\TestCase;

class GetAttacksUseCaseTest extends TestCase {

    /**
     * @var Attack[]
     */
    private $attacks;

    /**
     * @var IGetAttacksUseCase
     */
    private $useCase;

    /**
     * @var IGetAttacksUseCaseInput
     */
    private $useCaseInput;

    /**
     * @var IGetAttacksUseCaseOutput
     */
    private $useCaseOutput;

    protected function setUp() {
        $this->attacks = [
            new Attack(),
            new Attack(),
            new Attack()
        ];
        $entityGateway = $this->createMock(IGetManyAttackGateway::class);
        $entityGateway->method('execute')->willReturn($this->attacks);

        $this->useCase = new GetAttacksUseCase($entityGateway);
        $this->useCaseInput = new GetAttacksUseCaseInput();
        $this->useCaseOutput = new GetAttacksUseCaseOutput();
    }

    protected function tearDown() {
        $this->useCase = null;
    }

    public function testOutputsAttacks(): void {
        $this->useCase->execute($this->useCaseInput, $this->useCaseOutput);

        $actual = $this->useCaseOutput->getAttacks();
        $expected = $this->attacks;

        $this->assertEquals($expected, $actual);
    }
}