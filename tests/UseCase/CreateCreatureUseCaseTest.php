<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/10/2018
 * Time: 2:17 PM
 */

namespace App\Tests\UseCase;

use App\Entity\Attack;
use App\Entity\Color;
use App\Entity\Creature;
use App\Entity\Evolution;
use App\Entity\Level;
use App\Entity\Rarity;
use App\Entity\Size;
use App\EntityGateway\ICreateCreatureGateway;
use App\EntityGateway\IGetOneAttackGateway;
use App\EntityGateway\IGetOneColorGateway;
use App\EntityGateway\IGetOneCreatureGateway;
use App\EntityGateway\IGetOneEvolutionGateway;
use App\EntityGateway\IGetOneLevelGateway;
use App\EntityGateway\IGetOneRarityGateway;
use App\EntityGateway\IGetOneSizeGateway;
use App\UseCase\CreateCreatureUseCase;
use App\UseCase\CreateCreatureUseCaseInput;
use App\UseCase\CreateCreatureUseCaseOutput;
use App\UseCase\ICreateCreatureUseCase;
use App\UseCase\ICreateCreatureUseCaseInput;
use App\UseCase\ICreateCreatureUseCaseOutput;
use PHPUnit\Framework\TestCase;

class CreateCreatureUseCaseTest extends TestCase {

    /**
     * @var Creature
     */
    private $inputEntity;
    /**
     * @var Creature
     */
    private $outputEntity;
    /**
     * @var ICreateCreatureUseCase
     */
    private $useCase;
    /**
     * @var ICreateCreatureUseCaseInput
     */
    private $useCaseInput;
    /**
     * @var ICreateCreatureUseCaseOutput
     */
    private $useCaseOutput;

    protected function setUp() {
        $this->inputEntity = new Creature();
        $this->outputEntity = new Creature();

        $this->outputEntity->setId('12345');
        $this->inputEntity->setAttackRating(1);
        $this->outputEntity->setAttackRating(1);
        $this->inputEntity->setDefenseRating(2);
        $this->outputEntity->setDefenseRating(2);
        $this->inputEntity->setHealthPoints(3);
        $this->outputEntity->setHealthPoints(3);
        $this->inputEntity->setLuckRating(4);
        $this->outputEntity->setLuckRating(4);
        $this->inputEntity->setManaPool(5);
        $this->outputEntity->setManaPool(5);
        $this->inputEntity->setName('Mr Creature');
        $this->outputEntity->setName('Mr Creature');
        $this->inputEntity->setPowerRating(6);
        $this->outputEntity->setPowerRating(6);

        $attack = new Attack();
        $attack->setId('12345');
        $attack->setName('mr attack');
        $this->inputEntity->setAttack($attack);
        $this->outputEntity->setAttack($attack);

        $color = new Color();
        $color->setId('12345');
        $color->setName('red');
        $this->inputEntity->setColor($color);
        $this->outputEntity->setColor($color);

        $evolution = new Evolution();
        $evolution->setId('12345');
        $evolution->setCardinality(1);
        $this->inputEntity->setEvolution($evolution);
        $this->outputEntity->setEvolution($evolution);

        $level = new Level();
        $level->setId('12345');
        $level->setNumber(15);
        $this->inputEntity->setLevel($level);
        $this->outputEntity->setLevel($level);

        $nextCreature = new Creature();
        $nextCreature->setId('23456');
        $nextCreature->setName('Bit Mint');
        $this->inputEntity->setNextCreature($nextCreature);
        $this->outputEntity->setNextCreature($nextCreature);

        $previousCreature = new Creature();
        $previousCreature->setId('34567');
        $previousCreature->setName('Nibble Mint');
        $this->inputEntity->setPreviousCreature($previousCreature);
        $this->outputEntity->setPreviousCreature($previousCreature);

        $rarity = new Rarity();
        $rarity->setId('12345');
        $rarity->setName('common');
        $this->inputEntity->setRarity($rarity);
        $this->outputEntity->setRarity($rarity);

        $size = new Size();
        $size->setId('12345');
        $size->setName('small');
        $this->inputEntity->setSize($size);
        $this->outputEntity->setSize($size);

        $entityGateway = $this->createMock(ICreateCreatureGateway::class);
        $entityGateway->method('execute')->with($this->inputEntity)->willReturn($this->outputEntity);
        $getOneAttackGateway = $this->createMock(IGetOneAttackGateway::class);
        $getOneAttackGateway->method('getById')->with($attack->getId())->willReturn($attack);
        $getOneColorGateway = $this->createMock(IGetOneColorGateway::class);
        $getOneColorGateway->method('getById')->with($color->getId())->willReturn($color);
        $getOneCreatureGateway = $this->createMock(IGetOneCreatureGateway::class);
        $getOneCreatureGateway->expects($this->at(0))->method('getById')->with($nextCreature->getId())->willReturn(
            $nextCreature
        );
        $getOneCreatureGateway->expects($this->at(1))->method('getById')->with($previousCreature->getId())->willReturn(
            $previousCreature
        );
        $getOneEvolutionGateway = $this->createMock(IGetOneEvolutionGateway::class);
        $getOneEvolutionGateway->method('getById')->with($evolution->getId())->willReturn($evolution);
        $getOneLevelGateway = $this->createMock(IGetOneLevelGateway::class);
        $getOneLevelGateway->method('getById')->with($level->getId())->willReturn($level);
        $getOneRarityGateway = $this->createMock(IGetOneRarityGateway::class);
        $getOneRarityGateway->method('getById')->with($rarity->getId())->willReturn($rarity);
        $getOneSizeGateway = $this->createMock(IGetOneSizeGateway::class);
        $getOneSizeGateway->method('getById')->with($size->getId())->willReturn($size);

        $this->useCase = new CreateCreatureUseCase(
            $entityGateway,
            $getOneAttackGateway,
            $getOneColorGateway,
            $getOneCreatureGateway,
            $getOneEvolutionGateway,
            $getOneLevelGateway,
            $getOneRarityGateway,
            $getOneSizeGateway
        );
        $this->useCaseInput = new CreateCreatureUseCaseInput();
        $this->useCaseOutput = new CreateCreatureUseCaseOutput();

        $this->useCaseInput->setAttackId($this->inputEntity->getAttack()->getId());
        $this->useCaseInput->setAttackRating($this->inputEntity->getAttackRating());
        $this->useCaseInput->setColorId($this->inputEntity->getColor()->getId());
        $this->useCaseInput->setDefenseRating($this->inputEntity->getDefenseRating());
        $this->useCaseInput->setEvolutionId($this->inputEntity->getEvolution()->getId());
        $this->useCaseInput->setHealthPoints($this->inputEntity->getHealthPoints());
        $this->useCaseInput->setLevelId($this->inputEntity->getLevel()->getId());
        $this->useCaseInput->setLuckRating($this->inputEntity->getLuckRating());
        $this->useCaseInput->setManaPool($this->inputEntity->getManaPool());
        $this->useCaseInput->setName($this->inputEntity->getName());
        $this->useCaseInput->setNextCreatureId($this->inputEntity->getNextCreature()->getId());
        $this->useCaseInput->setPowerRating($this->inputEntity->getPowerRating());
        $this->useCaseInput->setPreviousCreatureId($this->inputEntity->getPreviousCreature()->getId());
        $this->useCaseInput->setRarityId($this->inputEntity->getRarity()->getId());
        $this->useCaseInput->setSizeId($this->inputEntity->getSize()->getId());
    }

    protected function tearDown() {
        $this->useCase = null;
        $this->useCaseInput = null;
        $this->useCaseOutput = null;
    }

    public function testOutputsExpectedValue() {
        $this->useCase->execute($this->useCaseInput, $this->useCaseOutput);

        $this->assertEquals($this->outputEntity, $this->useCaseOutput->getCreature());
    }
}