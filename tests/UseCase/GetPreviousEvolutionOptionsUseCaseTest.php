<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 7/7/2018
 * Time: 2:46 PM
 */

namespace App\Tests\UseCase;

use App\Entity\Creature;
use App\Entity\Evolution;
use App\EntityGateway\IGetPreviousEvolutionGateway;
use App\EntityGateway\IGetPreviousEvolutionOptionsGateway;
use App\UseCase\GetPreviousEvolutionOptionsUseCase;
use App\UseCase\GetPreviousEvolutionOptionsUseCaseInput;
use App\UseCase\GetPreviousEvolutionOptionsUseCaseOutput;
use App\UseCase\IGetPreviousEvolutionOptionsUseCase;
use App\UseCase\IGetPreviousEvolutionOptionsUseCaseInput;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetPreviousEvolutionOptionsUseCaseTest extends TestCase {

    /**
     * @var Creature[]
     */
    private $entities;
    /**
     * @var Evolution
     */
    private $evolution;
    /**
     * @var IGetPreviousEvolutionGateway|MockObject
     */
    private $getPreviousEvolutionGateway;
    /**
     * @var IGetPreviousEvolutionOptionsGateway|MockObject
     */
    private $getPreviousEvolutionOptionsGateway;
    /**
     * @var IGetPreviousEvolutionOptionsUseCase
     */
    private $useCase;
    /**
     * @var IGetPreviousEvolutionOptionsUseCaseInput
     */
    private $useCaseInput;

    protected function setUp() {
        $this->entities = [
            new Creature(),
            new Creature(),
            new Creature()
        ];
        $this->getPreviousEvolutionGateway = $this->createMock(IGetPreviousEvolutionGateway::class);
        $this->getPreviousEvolutionOptionsGateway = $this->createMock(IGetPreviousEvolutionOptionsGateway::class);

        $this->useCase = new GetPreviousEvolutionOptionsUseCase(
            $this->getPreviousEvolutionGateway,
            $this->getPreviousEvolutionOptionsGateway
        );
        $this->useCaseInput = new GetPreviousEvolutionOptionsUseCaseInput();

        $this->useCaseInput->setColorId('12345');
        $this->useCaseInput->setEvolutionId('23456');
        $this->useCaseInput->setSizeId('34567');

        $this->evolution = new Evolution();

        $this->evolution->setId('12345');
        $this->evolution->setCardinality(2);
    }

    protected function tearDown() {
        $this->entities = null;
        $this->getPreviousEvolutionOptionsGateway = null;
        $this->useCase = null;
        $this->useCaseInput = null;
    }

    public function testGetPreviousEvolutionGatewayIsCalled(): void {
        $useCaseOutput = new GetPreviousEvolutionOptionsUseCaseOutput();

        $this->getPreviousEvolutionGateway->expects($this->once())->method('execute')->with(
            $this->useCaseInput->getEvolutionId()
        );

        $this->useCase->execute($this->useCaseInput, $useCaseOutput);
    }

    public function testGetPreviousEvolutionOptionsGatewayIsCalled(): void {
        $useCaseOutput = new GetPreviousEvolutionOptionsUseCaseOutput();

        $this->getPreviousEvolutionGateway->method('execute')->willReturn($this->evolution);
        $this->getPreviousEvolutionOptionsGateway->expects($this->once())->method('execute')->withAnyParameters();

        $this->useCase->execute($this->useCaseInput, $useCaseOutput);
    }

    public function testGetPreviousEvolutionOptionsGatewayIsNotCalledWhenNoPreviousEvolutionIsReturned(): void {
        $useCaseOutput = new GetPreviousEvolutionOptionsUseCaseOutput();

        $this->getPreviousEvolutionGateway->method('execute')->willReturn(null);
        $this->getPreviousEvolutionOptionsGateway->expects($this->never())->method('execute')->withAnyParameters();

        $this->useCase->execute($this->useCaseInput, $useCaseOutput);
    }

    public function testUseCaseInputDataPassedIntoGetPreviousEvolutionOptionsGateway(): void {
        $useCaseOutput = new GetPreviousEvolutionOptionsUseCaseOutput();

        $this->getPreviousEvolutionGateway->method('execute')->willReturn($this->evolution);
        $this->getPreviousEvolutionOptionsGateway->expects($this->once())->method('execute')->with(
            $this->useCaseInput->getColorId(),
            $this->evolution->getId(),
            $this->useCaseInput->getSizeId()
        );

        $this->useCase->execute($this->useCaseInput, $useCaseOutput);
    }

    public function testUseCaseOutputIsPopulated(): void {
        $useCaseOutput = new GetPreviousEvolutionOptionsUseCaseOutput();

        $this->getPreviousEvolutionGateway->method('execute')->willReturn($this->evolution);
        $this->getPreviousEvolutionOptionsGateway->method('execute')->willReturn($this->entities);

        $this->useCase->execute($this->useCaseInput, $useCaseOutput);

        $actual = $useCaseOutput->getCreatures();
        $expected = $this->entities;

        $this->assertEquals($expected, $actual);
    }

}