<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 12:07 PM
 */

namespace App\Tests\EntityMapper;

use App\Entity\EntityFactory;
use App\Entity\Level;
use App\EntityMapper\EvolutionMapper;
use App\EntityMapper\IEntityMapper;
use App\EntityMapper\LevelMapper;
use App\EntityMapper\LevelRequirementMapper;
use App\EntityMapper\RarityMapper;
use App\EntityMapper\SizeMapper;
use PHPUnit\Framework\TestCase;

class LevelMapperTest extends TestCase {

    /**
     * @var string[][]
     */
    private $entities;

    /**
     * @var IEntityMapper
     */
    private $entityMapper;

    protected function setUp() {
        $this->entityMapper = new LevelMapper(
            new EntityFactory(),
            new LevelRequirementMapper(
                new EntityFactory(),
                new EvolutionMapper(
                    new EntityFactory(),
                    new RarityMapper(new EntityFactory()),
                    new SizeMapper(new EntityFactory())
                ),
                new SizeMapper(new EntityFactory())
            )
        );
        $this->entities = [
            [
                LevelMapper::$SourceKeyId => '12345',
                LevelMapper::$SourceKeyNumber => 15,
                LevelMapper::$SourceKeyLevelRequirements => [
                    [
                        LevelRequirementMapper::$SourceKeyId => '12345'
                    ],
                    [
                        LevelRequirementMapper::$SourceKeyId => '23456'
                    ]
                ]
            ],
            [
                LevelMapper::$SourceKeyId => '12345',
                LevelMapper::$SourceKeyNumber => 30,
                LevelMapper::$SourceKeyLevelRequirements => []
            ],
            [
                LevelMapper::$SourceKeyId => '12345',
                LevelMapper::$SourceKeyNumber => 45,
            ]
        ];
    }

    protected function tearDown() {
        $this->entityMapper = null;
    }

    public function testMapsId(): void {
        $entity = new Level();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getId();
        $expected = $this->entities[0][LevelMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsLevelRequirements(): void {
        $entity = new Level();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = count($entity->getLevelRequirements());
        $expected = count($this->entities[0][LevelMapper::$SourceKeyLevelRequirements]);

        $this->assertEquals($expected, $actual);
    }

    public function testMapsMany(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = count($entities);
        $expected = count($this->entities);

        $this->assertEquals($expected, $actual);
    }

    public function testMapsManyToExpectedType(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = get_class($entities[0]);
        $expected = Level::class;

        $this->assertEquals($expected, $actual);
    }

    public function testMapsNumber(): void {
        $entity = new Level();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getNumber();
        $expected = $this->entities[0][LevelMapper::$SourceKeyNumber];

        $this->assertEquals($expected, $actual);
    }

}