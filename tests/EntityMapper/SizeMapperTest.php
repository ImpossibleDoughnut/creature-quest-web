<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 12:07 PM
 */

namespace App\Tests\EntityMapper;

use App\Entity\EntityFactory;
use App\Entity\Size;
use App\EntityMapper\IEntityMapper;
use App\EntityMapper\SizeMapper;
use PHPUnit\Framework\TestCase;

class SizeMapperTest extends TestCase {

    /**
     * @var string[][]
     */
    private $entities;

    /**
     * @var IEntityMapper
     */
    private $entityMapper;

    protected function setUp() {
        $this->entityMapper = new SizeMapper(new EntityFactory());
        $this->entities = [
            [
                SizeMapper::$SourceKeyId => '12345',
                SizeMapper::$SourceKeyName => 'small'
            ],
            [
                SizeMapper::$SourceKeyId => '23456',
                SizeMapper::$SourceKeyName => 'medium'
            ],
            [
                SizeMapper::$SourceKeyId => '3456',
                SizeMapper::$SourceKeyName => 'large'
            ]
        ];
    }

    protected function tearDown() {
        $this->entityMapper = null;
    }

    public function testMapsId(): void {
        $entity = new Size();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getId();
        $expected = $this->entities[0][SizeMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsMany(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = count($entities);
        $expected = count($this->entities);

        $this->assertEquals($expected, $actual);
    }

    public function testMapsManyToExpectedType(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = get_class($entities[0]);
        $expected = Size::class;

        $this->assertEquals($expected, $actual);
    }

    public function testMapsName(): void {
        $entity = new Size();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getName();
        $expected = $this->entities[0][SizeMapper::$SourceKeyName];

        $this->assertEquals($expected, $actual);
    }

}