<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 12:07 PM
 */

namespace App\Tests\EntityMapper;

use App\Entity\Attack;
use App\Entity\EntityFactory;
use App\EntityMapper\AttackMapper;
use App\EntityMapper\IEntityMapper;
use PHPUnit\Framework\TestCase;

class AttackMapperTest extends TestCase {

    /**
     * @var string[][]
     */
    private $entities;

    /**
     * @var IEntityMapper
     */
    private $entityMapper;

    protected function setUp() {
        $this->entityMapper = new AttackMapper(new EntityFactory());
        $this->entities = [
            [
                AttackMapper::$SourceKeyId => '12345',
                AttackMapper::$SourceKeyName => 'bruiser'
            ],
            [
                AttackMapper::$SourceKeyId => '23456',
                AttackMapper::$SourceKeyName => 'mage'
            ],
            [
                AttackMapper::$SourceKeyId => '3456',
                AttackMapper::$SourceKeyName => 'soldier'
            ]
        ];
    }

    protected function tearDown() {
        $this->entityMapper = null;
    }

    public function testMapsId(): void {
        $entity = new Attack();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getId();
        $expected = $this->entities[0][AttackMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsMany(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = count($entities);
        $expected = count($this->entities);

        $this->assertEquals($expected, $actual);
    }

    public function testMapsManyToExpectedType(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = get_class($entities[0]);
        $expected = Attack::class;

        $this->assertEquals($expected, $actual);
    }

    public function testMapsName(): void {
        $entity = new Attack();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getName();
        $expected = $this->entities[0][AttackMapper::$SourceKeyName];

        $this->assertEquals($expected, $actual);
    }

}