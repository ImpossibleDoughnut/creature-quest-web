<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 12:07 PM
 */

namespace App\Tests\EntityMapper;

use App\Entity\EntityFactory;
use App\Entity\Evolution;
use App\EntityMapper\EvolutionMapper;
use App\EntityMapper\IEntityMapper;
use App\EntityMapper\RarityMapper;
use App\EntityMapper\SizeMapper;
use PHPUnit\Framework\TestCase;

class EvolutionMapperTest extends TestCase {

    /**
     * @var string[][]
     */
    private $entities;

    /**
     * @var IEntityMapper
     */
    private $entityMapper;

    protected function setUp() {
        $this->entityMapper = new EvolutionMapper(
            new EntityFactory(),
            new RarityMapper(new EntityFactory()),
            new SizeMapper(new EntityFactory())
        );
        $this->entities = [
            [
                EvolutionMapper::$SourceKeyId => '12345',
                EvolutionMapper::$SourceKeyCardinality => 1,
                EvolutionMapper::$SourceKeyRarities => [
                    [
                        RarityMapper::$SourceKeyId => '12345',
                        RarityMapper::$SourceKeyName => 'Mr Rarity'
                    ]
                ],
                EvolutionMapper::$SourceKeySizes => [
                    [
                        SizeMapper::$SourceKeyId => '12345',
                        SizeMapper::$SourceKeyName => 'Mr Size'
                    ],
                    [
                        SizeMapper::$SourceKeyId => '12345',
                        SizeMapper::$SourceKeyName => 'Mrs Size'
                    ]
                ],
            ],
            [
                EvolutionMapper::$SourceKeyId => '23456',
                EvolutionMapper::$SourceKeyCardinality => 2,
                EvolutionMapper::$SourceKeyRarities => [],
                EvolutionMapper::$SourceKeySizes => [],
            ],
            [
                EvolutionMapper::$SourceKeyId => '3456',
                EvolutionMapper::$SourceKeyCardinality => 3,
                EvolutionMapper::$SourceKeyRarities => [],
                EvolutionMapper::$SourceKeySizes => [],
            ]
        ];
    }

    protected function tearDown() {
        $this->entityMapper = null;
    }

    public function testMapsCardinality(): void {
        $entity = new Evolution();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getCardinality();
        $expected = $this->entities[0][EvolutionMapper::$SourceKeyCardinality];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsId(): void {
        $entity = new Evolution();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getId();
        $expected = $this->entities[0][EvolutionMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsMany(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = count($entities);
        $expected = count($this->entities);

        $this->assertEquals($expected, $actual);
    }

    public function testMapsManyToExpectedType(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = get_class($entities[0]);
        $expected = Evolution::class;

        $this->assertEquals($expected, $actual);
    }

    public function testMapsRarities(): void {
        $entity = new Evolution();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = count($entity->getRarities());
        $expected = count($this->entities[0][EvolutionMapper::$SourceKeyRarities]);

        $this->assertEquals($expected, $actual);
    }

    public function testMapsSizes(): void {
        $entity = new Evolution();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = count($entity->getSizes());
        $expected = count($this->entities[0][EvolutionMapper::$SourceKeySizes]);

        $this->assertEquals($expected, $actual);
    }

}