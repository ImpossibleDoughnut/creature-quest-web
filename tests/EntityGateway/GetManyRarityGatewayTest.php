<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\Tests\EntityGateway;

use App\ApiRequest\ApiRequestConfigurationFactory;
use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\EntityFactory;
use App\Entity\IEntityFactory;
use App\EntityGateway\GetManyRarityGateway;
use App\EntityGateway\IGetManyRarityGateway;
use App\EntityMapper\RarityMapper;
use PHPUnit\Framework\TestCase;

class GetManyRarityGatewayTest extends TestCase {

    /**
     * @var IApiReadRequest
     */
    private $apiRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var IEntityFactory
     */
    private $entityFactory;
    /**
     * @var RarityMapper
     */
    private $entityMapper;
    /**
     * @var IGetManyRarityGateway
     */
    private $getManyEntityGateway;

    protected function setUp() {
        $this->apiRequest = $this->createMock(IApiReadRequest::class);
        $this->apiRequestConfigurationFactory = new ApiRequestConfigurationFactory();
        $this->entityFactory = new EntityFactory();
        $this->entityMapper = new RarityMapper($this->entityFactory);

        $apiRequestResponse = [
            'rarities' => [
                [
                    RarityMapper::$SourceKeyId => '12345',
                    RarityMapper::$SourceKeyName => 'Mr Rarity'
                ]
            ]
        ];
        $this->apiRequest->method('execute')->willReturn($apiRequestResponse);

        $this->getManyEntityGateway = new GetManyRarityGateway(
            $this->apiRequest,
            $this->apiRequestConfigurationFactory,
            $this->entityFactory,
            $this->entityMapper
        );
    }

    protected function tearDown() {
        $this->getManyEntityGateway = null;
    }

    public function testReturnsExpectedData() {
        $actual = count($this->getManyEntityGateway->execute());
        $expected = 1;

        $this->assertEquals($expected, $actual);
    }

}