<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\Tests\EntityGateway;

use App\Entity\Attack;
use App\EntityGateway\GetOneAttackGateway;
use App\EntityGateway\IGetManyAttackGateway;
use App\EntityGateway\IGetOneAttackGateway;
use App\EntityMapper\AttackMapper;
use PHPUnit\Framework\TestCase;

class GetOneAttackGatewayTest extends TestCase {

    /**
     * @var Attack[]
     */
    private $attacks;
    /**
     * @var AttackMapper
     */
    private $getManyEntityGateway;
    /**
     * @var IGetOneAttackGateway
     */
    private $getOneEntityGateway;

    protected function setUp() {
        $this->getManyEntityGateway = $this->createMock(IGetManyAttackGateway::class);
        $this->attacks = [];

        $attack = new Attack();
        $attack->setId('12345');
        $attack->setName('Mr Attack');
        $this->attacks[] = $attack;

        $this->getManyEntityGateway->method('execute')->willReturn($this->attacks);

        $this->getOneEntityGateway = new GetOneAttackGateway($this->getManyEntityGateway);
    }

    protected function tearDown() {
        $this->getOneEntityGateway = null;
    }

    public function testReturnsExpectedData() {
        $actual = $this->getOneEntityGateway->getById($this->attacks[0]->getId());

        $this->assertEquals($this->attacks[0], $actual);
    }

}