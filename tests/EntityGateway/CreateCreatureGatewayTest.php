<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/10/2018
 * Time: 2:20 PM
 */

namespace App\Tests\EntityGateway;

use App\ApiRequest\IApiCreateRequest;
use App\ApiRequest\IApiCreateRequestConfiguration;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\Attack;
use App\Entity\Color;
use App\Entity\Creature;
use App\Entity\Evolution;
use App\Entity\IEntityFactory;
use App\Entity\Level;
use App\Entity\Rarity;
use App\Entity\Size;
use App\EntityGateway\CreateCreatureGateway;
use App\EntityGateway\ICreateCreatureGateway;
use App\EntityMapper\CreatureMapper;
use InvalidArgumentException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CreateCreatureGatewayTest extends TestCase {

    /**
     * @var IApiCreateRequest|MockObject
     */
    private $apiRequest;
    /**
     * @var IApiCreateRequestConfiguration|MockObject
     */
    private $apiRequestConfiguration;
    /**
     * @var IApiRequestConfigurationFactory|MockObject
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var string[]
     */
    private $apiRequestResponse;
    /**
     * @var Creature
     */
    private $entity;
    /**
     * @var IEntityFactory|MockObject
     */
    private $entityFactory;
    /**
     * @var ICreateCreatureGateway
     */
    private $entityGateway;
    /**
     * @var CreatureMapper|MockObject
     */
    private $entityMapper;
    /**
     * @var Creature|MockObject
     */
    private $entityMock;

    protected function setUp() {
        $this->apiRequest = $this->createMock(IApiCreateRequest::class);
        $this->apiRequestConfigurationFactory = $this->createMock(IApiRequestConfigurationFactory::class);
        $this->apiRequestConfiguration = $this->createMock(IApiCreateRequestConfiguration::class);
        $this->entityFactory = $this->createMock(IEntityFactory::class);
        $this->entityMapper = $this->createMock(CreatureMapper::class);
        $this->entityMock = $this->createMock(Creature::class);

        $this->apiRequestConfigurationFactory->method('createCreateConfiguration')->willReturn(
            $this->apiRequestConfiguration
        );
        $this->entityFactory->method('create')->willReturn($this->entityMock);

        $this->apiRequestResponse = [
            'creature' => [
                CreatureMapper::$SourceKeyId => '12345',
                CreatureMapper::$SourceKeyName => 'Mr. Creature'
            ]
        ];

        $this->apiRequest->method('execute')->willReturn($this->apiRequestResponse);

        $this->entity = new Creature();
        $this->entity->setId('12345');
        $this->entity->setAttackRating(1);
        $this->entity->setDefenseRating(2);
        $this->entity->setHealthPoints(3);
        $this->entity->setLuckRating(4);
        $this->entity->setManaPool(5);
        $this->entity->setName('Mr Creature');
        $this->entity->setPowerRating(6);

        $attack = new Attack();
        $attack->setId('12345');
        $attack->setName('mr attack');
        $this->entity->setAttack($attack);

        $color = new Color();
        $color->setId('12345');
        $color->setName('red');
        $this->entity->setColor($color);

        $evolution = new Evolution();
        $evolution->setId('12345');
        $evolution->setCardinality(1);
        $this->entity->setEvolution($evolution);

        $level = new Level();
        $level->setId('12345');
        $level->setNumber(15);
        $this->entity->setLevel($level);

        $nextCreature = new Creature();
        $nextCreature->setId('23456');
        $nextCreature->setName('Jimmy');
        $this->entity->setNextCreature($nextCreature);

        $previousCreature = new Creature();
        $previousCreature->setId('34567');
        $previousCreature->setName('Jim');
        $this->entity->setPreviousCreature($previousCreature);

        $rarity = new Rarity();
        $rarity->setId('12345');
        $rarity->setName('common');
        $this->entity->setRarity($rarity);

        $size = new Size();
        $size->setId('12345');
        $size->setName('small');
        $this->entity->setSize($size);

        $this->entityGateway = new CreateCreatureGateway(
            $this->apiRequest,
            $this->apiRequestConfigurationFactory,
            $this->entityFactory,
            $this->entityMapper
        );
    }

    protected function tearDown() {
        $this->entityGateway = null;
    }

    public function testApiRequestConfigurationFactoryIsCalled() {
        $parameters = [
            CreateCreatureGateway::PARAMETER_ATTACK_ID => $this->entity->getAttack()->getId(),
            CreateCreatureGateway::PARAMETER_ATTACK_RATING => $this->entity->getAttackRating(),
            CreateCreatureGateway::PARAMETER_COLOR_ID => $this->entity->getColor()->getId(),
            CreateCreatureGateway::PARAMETER_DEFENSE_RATING => $this->entity->getDefenseRating(),
            CreateCreatureGateway::PARAMETER_EVOLUTION_ID => $this->entity->getEvolution()->getId(),
            CreateCreatureGateway::PARAMETER_HEALTH_POINTS => $this->entity->getHealthPoints(),
            CreateCreatureGateway::PARAMETER_LEVEL_ID => $this->entity->getLevel()->getId(),
            CreateCreatureGateway::PARAMETER_LUCK_RATING => $this->entity->getLuckRating(),
            CreateCreatureGateway::PARAMETER_MANA_POOL => $this->entity->getManaPool(),
            CreateCreatureGateway::PARAMETER_NAME => $this->entity->getName(),
            CreateCreatureGateway::PARAMETER_NEXT_CREATURE_ID => $this->entity->getNextCreature()->getId(),
            CreateCreatureGateway::PARAMETER_POWER_RATING => $this->entity->getPowerRating(),
            CreateCreatureGateway::PARAMETER_PREVIOUS_CREATURE_ID => $this->entity->getPreviousCreature()->getId(),
            CreateCreatureGateway::PARAMETER_RARITY_ID => $this->entity->getRarity()->getId(),
            CreateCreatureGateway::PARAMETER_SIZE_ID => $this->entity->getSize()->getId()
        ];
        $this->apiRequestConfigurationFactory->expects($this->once())->method('createCreateConfiguration')->with(
            'creatures',
            $parameters
        );

        $this->entityGateway->execute($this->entity);
    }

    public function testApiRequestIsMade() {
        $this->apiRequest->expects($this->once())->method('execute')->with($this->apiRequestConfiguration);

        $this->entityGateway->execute($this->entity);
    }

    public function testEntityFactoryIsCalled() {
        $this->entityFactory->expects($this->once())->method('create')->with(Creature::class);

        $this->entityGateway->execute($this->entity);
    }

    public function testEntityMapperIsCalled() {
        $this->entityMapper->expects($this->once())->method('mapOne')->with(
            $this->apiRequestResponse['creature'],
            $this->entityMock
        );

        $this->entityGateway->execute($this->entity);
    }

    public function testReturnsEntityThatIsNotNull() {
        $actual = $this->entityGateway->execute($this->entity);

        $this->assertNotNull($actual);
    }

    public function testThrowsExceptionIfMissingAttack() {
        $entity = new Creature();
        $entity->setColor($this->entity->getColor());
        $entity->setEvolution($this->entity->getEvolution());
        $entity->setLevel($this->entity->getLevel());
        $entity->setName($this->entity->getName());
        $entity->setRarity($this->entity->getRarity());
        $entity->setSize($this->entity->getSize());

        $this->expectException(InvalidArgumentException::class);
        $this->entityGateway->execute($entity);
    }

    public function testThrowsExceptionIfMissingColor() {
        $entity = new Creature();
        $entity->setAttack($this->entity->getAttack());
        $entity->setEvolution($this->entity->getEvolution());
        $entity->setLevel($this->entity->getLevel());
        $entity->setName($this->entity->getName());
        $entity->setRarity($this->entity->getRarity());
        $entity->setSize($this->entity->getSize());

        $this->expectException(InvalidArgumentException::class);
        $this->entityGateway->execute($entity);
    }

    public function testThrowsExceptionIfMissingEvolution() {
        $entity = new Creature();
        $entity->setAttack($this->entity->getAttack());
        $entity->setColor($this->entity->getColor());
        $entity->setLevel($this->entity->getLevel());
        $entity->setName($this->entity->getName());
        $entity->setRarity($this->entity->getRarity());
        $entity->setSize($this->entity->getSize());

        $this->expectException(InvalidArgumentException::class);
        $this->entityGateway->execute($entity);
    }

    public function testThrowsExceptionIfMissingLevel() {
        $entity = new Creature();
        $entity->setAttack($this->entity->getAttack());
        $entity->setColor($this->entity->getColor());
        $entity->setEvolution($this->entity->getEvolution());
        $entity->setName($this->entity->getName());
        $entity->setRarity($this->entity->getRarity());
        $entity->setSize($this->entity->getSize());

        $this->expectException(InvalidArgumentException::class);
        $this->entityGateway->execute($entity);
    }

    public function testThrowsExceptionIfMissingName() {
        $entity = new Creature();
        $entity->setAttack($this->entity->getAttack());
        $entity->setColor($this->entity->getColor());
        $entity->setEvolution($this->entity->getEvolution());
        $entity->setLevel($this->entity->getLevel());
        $entity->setRarity($this->entity->getRarity());
        $entity->setSize($this->entity->getSize());

        $this->expectException(InvalidArgumentException::class);
        $this->entityGateway->execute($entity);
    }

    public function testThrowsExceptionIfMissingRarity() {
        $entity = new Creature();
        $entity->setAttack($this->entity->getAttack());
        $entity->setColor($this->entity->getColor());
        $entity->setEvolution($this->entity->getEvolution());
        $entity->setLevel($this->entity->getLevel());
        $entity->setName($this->entity->getName());
        $entity->setSize($this->entity->getSize());

        $this->expectException(InvalidArgumentException::class);
        $this->entityGateway->execute($entity);
    }

    public function testThrowsExceptionIfMissingSize() {
        $entity = new Creature();
        $entity->setAttack($this->entity->getAttack());
        $entity->setColor($this->entity->getColor());
        $entity->setEvolution($this->entity->getEvolution());
        $entity->setLevel($this->entity->getLevel());
        $entity->setName($this->entity->getName());
        $entity->setRarity($this->entity->getRarity());

        $this->expectException(InvalidArgumentException::class);
        $this->entityGateway->execute($entity);
    }

}