<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 12:54 PM
 */

namespace App\Tests\ApiRequest;

use App\ApiRequest\ApiRequestConfigurationFactory;
use App\ApiRequest\IApiRequestConfigurationFactory;
use PHPUnit\Framework\TestCase;

class ApiRequestConfigurationFactoryTest extends TestCase {

    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var string
     */
    private $endpointUrlSuffix;
    /**
     * @var array
     */
    private $parameters;

    protected function setUp() {
        $this->apiRequestConfigurationFactory = new ApiRequestConfigurationFactory();
        $this->endpointUrlSuffix = 'entity';
        $this->parameters = [
            'key' => 'value'
        ];
    }

    protected function tearDown() {
        $this->apiRequestConfigurationFactory = null;
        $this->endpointUrlSuffix = null;
        $this->parameters = null;
    }

    public function testCreatesCreateConfiguration() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createCreateConfiguration(
            $this->endpointUrlSuffix
        );

        $this->assertNotNull($apiRequestConfiguration);
    }

    public function testCreatesCreateConfigurationAndSetsEndpointUrlSuffix() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createCreateConfiguration(
            $this->endpointUrlSuffix
        );

        $this->assertEquals($this->endpointUrlSuffix, $apiRequestConfiguration->getEndpointUrlSuffix());
    }

    public function testCreatesCreateConfigurationAndSetsParameters() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createCreateConfiguration(
            $this->endpointUrlSuffix,
            $this->parameters
        );

        $this->assertEquals($this->parameters, $apiRequestConfiguration->getParameters());
    }

    public function testCreatesDeleteConfiguration() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createDeleteConfiguration(
            $this->endpointUrlSuffix
        );

        $this->assertNotNull($apiRequestConfiguration);
    }

    public function testCreatesDeleteConfigurationAndSetsEndpointUrlSuffix() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createDeleteConfiguration(
            $this->endpointUrlSuffix
        );

        $this->assertEquals($this->endpointUrlSuffix, $apiRequestConfiguration->getEndpointUrlSuffix());
    }

    public function testCreatesDeleteConfigurationAndSetsParameters() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createDeleteConfiguration(
            $this->endpointUrlSuffix,
            $this->parameters
        );

        $this->assertEquals($this->parameters, $apiRequestConfiguration->getParameters());
    }

    public function testCreatesReadConfiguration() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createReadConfiguration(
            $this->endpointUrlSuffix
        );

        $this->assertNotNull($apiRequestConfiguration);
    }

    public function testCreatesReadConfigurationAndSetsEndpointUrlSuffix() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createReadConfiguration(
            $this->endpointUrlSuffix
        );

        $this->assertEquals($this->endpointUrlSuffix, $apiRequestConfiguration->getEndpointUrlSuffix());
    }

    public function testCreatesReadConfigurationAndSetsParameters() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createReadConfiguration(
            $this->endpointUrlSuffix,
            $this->parameters
        );

        $this->assertEquals($this->parameters, $apiRequestConfiguration->getParameters());
    }

    public function testCreatesUpdateConfiguration() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createUpdateConfiguration(
            $this->endpointUrlSuffix
        );

        $this->assertNotNull($apiRequestConfiguration);
    }

    public function testCreatesUpdateConfigurationAndSetsEndpointUrlSuffix() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createUpdateConfiguration(
            $this->endpointUrlSuffix
        );

        $this->assertEquals($this->endpointUrlSuffix, $apiRequestConfiguration->getEndpointUrlSuffix());
    }

    public function testCreatesUpdateConfigurationAndSetsParameters() {
        $apiRequestConfiguration = $this->apiRequestConfigurationFactory->createUpdateConfiguration(
            $this->endpointUrlSuffix,
            $this->parameters
        );

        $this->assertEquals($this->parameters, $apiRequestConfiguration->getParameters());
    }

}